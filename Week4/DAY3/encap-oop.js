class Bank {
    constructor() {
        this.saldo = 1000;
    }

}

class Atm extends Bank{
    constructor() {
        super();
    }
    withdraw() {}
    transfer() {}
    getSaldo() {}
}

const atm = new Atm ();
console.log(atm.saldo);
