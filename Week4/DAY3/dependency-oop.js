class Car {
    checkWheel() {
        console.log("ada 4 roda");
    }
}
class Motorcycle {
    checkWheel() {
        console.log("ada 2 roda");
    }
}

class Engine {
    checkEngine() {
        console.log("engine is fine");
    }
}
class Honda {
    constructor(type, car, motorcycle, engine) {
        this.type = type;
        this.car = dependency.car;
        this.motorcycle = dependency.motorcycle;
        this.engine = dependency.engine;
    }
}

const car = new Car();
const motorcycle = new Motorcycle();
const engine = new Engine();

const dependency = {car, motorcycle, engine};

const mobilio = new Honda("Mobilio", dependency);

mobilio.car.checkWheel();
mobilio.motorcycle.checkWheel();
mobilio.engine.checkEngine();