//Produsen
class Car {
    constructor() {
        // Public Property
        this.brand = "Honda";
    }
    // Public Method
    getBrand() {
        console.log("brandnya adalah " + this.brand);
    }
}

// Client
const car = new Car();
car.brand;
console.log(car.brand);