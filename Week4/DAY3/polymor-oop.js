// Parents class / super class

class Engine {
  startEngine() {
    console.log(`Mesin ${this.type} menyala`);
  }
  stopEngine() {}
}

// Child class / sub cLASS
class Car extends Engine {
  horn() {} // one function can be used on many class (POlymorphysm)
}

// Child class / sub class
class Toyota extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

class Honda extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

class Daihatsu extends Car {
  constructor(type) {
    super();
    this.type = type;
  }
}

// const avanza = new Toyota();

// avanza.startEngine();

// const mobilio = new Honda();

// mobilio.startEngine();

// const xpander = new Daihatsu();

// xpander.startEngine();

const avanza = new Toyota("Avanza");
const mobilio = new Honda("Mobilio");
const xpander = new Daihatsu("X-pander");

avanza.startEngine();
mobilio.startEngine();
xpander.startEngine();
