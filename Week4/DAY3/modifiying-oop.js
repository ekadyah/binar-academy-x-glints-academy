class Car {
    constructor() {
        this.brand = "Suzuki";
    }
    startEngine() {
        console.log("mesin menyala");
    }
}

const car = new Car();

// Modifiying atau overidding

car.brand = "Honda";
car.startEngine = function() {
    console.log("METHODNYA AKU BAJAK HAHAHA");
};

console.log(car.brand);
car.startEngine();