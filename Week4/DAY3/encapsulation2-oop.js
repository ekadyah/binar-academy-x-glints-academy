// class Bank {
//     constructor() {
//         this.saldo = 1000;
//     }

// }

// class Atm extends Bank{
//     constructor() {
//         super();
//     }
//     withdraw() {}
//     transfer() {}
//     getSaldo() {}
// }

// const atm = new Atm ();
// console.log(atm.saldo);


class Bank {
    #saldo = 1000;
    getSaldo() {
        console.log(`${this.#saldo}`);
    }
    #setSaldo(saldo){
        this.#saldo += saldo;
    }
    deposit(saldo) {
        this.#setSaldo(saldo);
    }
}

class Atm extends Bank{
    constructor() {
        super();
    }
    withdraw() {}
    transfer() {}
    payment() {}
    }


const atm = new Atm ();

atm.deposit(4000);
atm.getSaldo();
