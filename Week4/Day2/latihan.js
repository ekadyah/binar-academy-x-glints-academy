class Mouse {
    constructor(nama, warna, ukuran, klik) {
    this.name = nama;
    this.color = warna;
    this.size = ukuran;
    this.clik = klik;
    }

startMouse() {
    console.log(`Mouse ${this.name} yang tipe kliknya ${this.click} siap digunakan`);
}

stopMouse() {
    console.log(`Mouse ${this.name} yang berwarna ${this.color} telah berhenti digunakan`);
}
}

const nama = "Logitech";
const warna = "black";
const ukuran = "small";
const klik = "silent";

const mouse = new Mouse(nama, warna, ukuran, klik);

mouse.startMouse();
