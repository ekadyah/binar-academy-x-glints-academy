const mobil = {
    name : "Toyota Avanza",
    weight: 800,
    model: "MPV",
    color: "putih",
    start: function() {
        console.log(`${this.name} telah menyala`);
    },
    drive: function() {
        console.log(`${this["name"]} siap dikemudikan`);
    },
    brake: function() {
        console.log("mobil telah direm");
    },
    stop: function() {
        console.log("mobil telah berhenti");
    },
};

// memanggil property
console.log(mobil.name);
console.log(mobil['name']);
console.log(mobil["model"]);

// memanggil method
mobil.start();
mobil.drive();
mobil.brake();
mobil.stop();
mobil["start"]();
mobil["drive"]();

// input nilai baru
mobil.name = "Honda Jazz";
mobil.start();

// mengisi fungsi yang baru
// mobil.start = function () {
//     console.log("fungsi baru, fungsi lama tidak berlaku")
// }

// mobil.start();