class Car {
  // static properthy
  static brand2 = "Honda";
  constructor() {
    // Instance Properthy
    this.brand = "Toyota";
    this.type = "Honda";
  }
  // Instance Method
  startEngine() {
    console.log("mobil menyalakan mesin");
  }
  static getBrand2() {
    return Car.brand2;
  }
}

const car = new Car();

// console.log(car.brand);
// car.startEngine();

console.log(Car.brand2); // memanggil property static
console.log(Car.getBrand2());
