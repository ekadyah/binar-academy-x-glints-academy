class Mobil {
    // inisiasi property dalam object class
    constructor(name, weight, color) {
    this.name = name;
    this.weight= weight;
    this.model = "MPV";
    this.color = color;
}

start() {
    console.log(`${this.name} telah menyala`);
    }
drive() {
    console.log(`${this["name"]} siap dikemudikan`);
    }
brake() {
    console.log("mobil telah direm");
    }
stop() {
    console.log("mobil telah berhenti");
    }

    changeName(name) {
        this.name = name;
    }
    changeWeight(weight) {
        this.weight = weight;
    }
    changeColor(color) {
        this.color = color;
    }    
}

// create new instatance
// instance adalah kelas
// const mobil = new Mobil();
// console.log(mobil.name); // memanggil nama mobil
// mobil.drive(); // menjalankan fungsi drive
// mobil.name = "Honda Jazz"; // mengganti nama mobil
// mobil.start = function() {
//     console.log("ini fungsi saya replace");
// }
// mobil.start();

// membuat 2 const
const mobil = new Mobil("Honda jazz", "merah");
const mobil2 = new Mobil("Suzuki ertiga", "hitam");

mobil.changeName("Suzuki Jimmy");
mobil.changeWeight(500);
mobil.changeColor("Hijau DOngker")

mobil.start();
mobil2.start();