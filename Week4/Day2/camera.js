class Camera {
  // inisiasi property dalam object class
  constructor(nama, berat, tipe, lensa) {
    this.name = nama;
    this.weight = berat;
    this.type = tipe;
    this.lens = lensa;
  }

  startCamera() {
    console.log(`Camera ${this.name} dengan ${this.type} siap digunakan`);
  }

  stopCamera() {
    console.log(`Camera ${this.name} telah berhenti `);
  }

  videoCamera() {
    console.log(
      `merk ${this.name} memiliki berat ${this.weight} dengan tipe ${this.type} dan lensa ${this.lens} kualitas videonya FHD`
    );
  }

  gambarCamera() {
    console.log(
      `merk ${this.name} memiliki berat ${this.weight} dengan tipe ${this.type} dan lensa ${this.lens} kualitas videonya HD`
    );
  }

  changeLens(lensaBaru) {
    this.lens = lensaBaru;
  }
  changeType(tipeBaru) {
    this.type = tipeBaru;
  }
}

const nama = "Canon";
const berat = 800;
const tipe = "DSLR";
const lensa = "kit";

const camera = new Camera(nama, berat, tipe, lensa);
camera.changeLens("Fixed");
camera.changeType("Mirroless");

camera.videoCamera();
camera.gambarCamera();
