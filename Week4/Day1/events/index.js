const Event = require("events");

// Inisiasi sebuah variabel event

const eventEmitter = new Event();

// inisiasi function handler
function lampuMerah() {
    console.log("kendaraan anda harus berhenti");
}
function lampuHijau() {
    console.log("kendaraan anda harus jalan");
}    
// Registrasi sebuah event
eventEmitter.on("Lampumerah", lampuMerah)
eventEmitter.on("Lampuhijau", lampuHijau)

// trigger events
eventEmitter.emit("Lampumerah");
