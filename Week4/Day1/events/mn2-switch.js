const Event = require("events");

// Inisiasi sebuah variabel event

const eventEmitter = new Event();

// inisiasi function handler
function airMendidih(suhu) {
    console.log(suhu + ' adalah air mendidih');
}

function airHangat(suhu) {
    console.log(suhu + " adalah air hangat");
}

function airBeku(suhu) {
    console.log(suhu + "adalah air hangat");
}

// Registrasi sebuah event
eventEmitter.on("air mendidih", airMendidih)
eventEmitter.on("air hangat", airHangat)
eventEmitter.on("air beku", airBeku)

for (let celcius = 200; celcius > -2; celcius--) {
    // switch (celcius) {
    //     case 100:
    //         eventEmitter.emit('air mendidih', celcius);
    //         break;
    //     case 50:
    //         eventEmitter.emit('air hangat', celcius);
    //         break; 
    //     case 0:
    //         eventEmitter.emit('air beku', celcius);
    //         break;              
    //     default:
    //         break;
    // }   
// }
