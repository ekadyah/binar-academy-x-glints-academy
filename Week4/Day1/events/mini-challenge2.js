const Event = require("events");

// Inisiasi sebuah variabel event

const eventEmitter = new Event();

// inisiasi function handler
function airMendidih(number) {
    console.log(number + 'air mendidih');
}

function airHangat(number) {
    console.log(number + "air hangat");
}

function airBeku(number) {
    console.log(number + "air hangat");
}

// Registrasi sebuah event
eventEmitter.on("air mendidih", airMendidih)
eventEmitter.on("air hangat", airHangat)
eventEmitter.on("air beku", airBeku)

for (let celcius = 200; celcius > -2; celcius--) {
    if (celcius == 100){
        eventEmitter.emit("air mendidih", celcius);
    } else if (celcius == 50) {
        eventEmitter.emit("air hangat", celcius);
    } else if (celcius == 0){
        eventEmitter.emit("air beku", celcius)
       }     
}
