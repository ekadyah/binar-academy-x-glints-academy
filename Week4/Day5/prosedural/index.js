// prosedural programming adalah meulis fungsi satu persatu dan memanggil satu persatu,
// masih manual karena menulis baris perbaris

const menghitungLuasSegitiga = (a, t) => 0.5 * a * t; // jika hanya satu baris bisa dipersingkat seperti ini

// sama aja dengan penulisan karena antara baris trkhir dgn arrow function hanya satu dpt dipersingkat sprti di atas,
// return sudah bermakna arrow function
// const menghitungLuasSegitiga = (a,t) => {
//    return 0.5 * a * t;
//};

const menghitungLuasPersegi = (s) => s * s;

const menghitungLuasPersegiPanjang = (p, l) => p * l;

const menghitungLuasLingkaran = (r) => 3.14 * r * r;

console.log(`luas segitiga ${menghitungLuasSegitiga(10, 10)}`);
console.log(`luas persegi ${menghitungLuasPersegi(10, 10)}`);
console.log(`luas persegi panjang ${menghitungLuasPersegiPanjang(10, 10)}`);
console.log(`luas lingkaran ${menghitungLuasLingkaran(10, 10)}`);
