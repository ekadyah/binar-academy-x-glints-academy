const express = require("express");
const bodyParser = require("body-parser");
const app = express();

// Parsing request body row json
app.use(bodyParser.json());
// Parsing request body x-www-form-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

app.get("/", (req, res) => {
  res.json({ message: "hello world" });
});

app.get("*", (req, res) => {
  res.status(404).json({ message: "routing tidak ditemukan" });
});

app.listen(5000, () => console.log("app running on port 5000"));
