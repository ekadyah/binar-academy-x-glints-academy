const data = ['apple', 'orange', 'banana']

const data2 = {id: 1, name:"Andre", year:1999, jk:'Pria'}

const data3 = [
  {id: 1, name:"Andre", year:1999, jk:'Pria'}, //0
  {id: 2, name:"Bayu", year:1992, jk:'Pria'}   //1
]

const data4 = [
  // 0 
  [
    // 0
    {
      id: 1,
      name: "Triagung",
      year: 1998
    }
  ],
  // 1
  [
    // 0
    {
      id: 2,
      name: "Tamam",
      year: 1997
    }
  ]
]

console.log(data4[0][0].name)

console.log(data4[1][0].name)