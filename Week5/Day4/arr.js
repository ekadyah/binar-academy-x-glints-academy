// 1. for each
// const array = [1, 2, 3, 4, 5]

// array.forEach(item => { 
//   console.log(item);  // Output: 1 2 3 4 5
// });


//2. filter 
//  membuat array baru dari array angka yang isinya adalah bilangan habis dibagi 2
// const angka = [1, 2, 3, 4, 5, 6, 7, 8, 9]

// const filteredArray = angka.filter(item => item % 2 === 0);

// console.log(filteredArray) // Output: [2, 4, 6, 8]

// 3. map
// const angka = [1, 2, 3, 4, 5, 6, 7, 8, 9]

// // membuat array baru dari array angka untuk memeriksa apakah setiap elemennya bernilai habis dibagi 2 atau tidak
// const mapedArray = angka.map(item => item % 2 === 0);
// console.log(mapedArray); // output: [false, true, false, true, false, true, false, true, false]

// // membuat array baru dari array angka untuk melakukan operasi perkalian 2 pada setiap elemennya
// const multipleOfTwo = angka.map(e => e * 2);
// console.log(multipleOfTwo); // Output: [2, 4, 6, 8, 10, 12, 14, 16, 18]

// 4. includes ()
// const angka = [1, 2, 3, 4, 5, 6, 7, 8, 9]

// const something = angka.includes(2);
// const any = angka.includes(10);

// console.log(something); // Output: true
// console.log(any); // Output: false

// 5. some()
const angka = [1, 2, 3, 4, 5]

// mengecek apakah dalam array angka terdapat elemen yang habis dibagi 2
const some = angka.some(item => item % 2);
console.log(some); // Output: true

// mengecek apakah dalam array angka terdapat elemen yang kurang dari 0
const thing = angka.some(item => item < 0);
console.log(thing); // Output: false