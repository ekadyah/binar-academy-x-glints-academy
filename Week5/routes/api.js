const router = require("express").Router();

module.exports = router;

// routing users
// create users
router.post("/users", (req, res) => {
  res.send("routing untuk create user");
});

// Read users
router.get("/users", (req, res) => {
  res.send("routing untuk read user");
});

// update users
// put untuk edit atau mengganti semuanya
// patch mengganti satu bagian

router.put("/users", (req, res) => {
  res.send("routing untuk update user");
});

//  delete users
app.delete("/users", (req, res) => {
  res.send("routing untuk delete user");
});

// routing tasks
// create tasks
router.post("/tasks", (req, res) => {
    res.send("routing untuk create tasks");
  });
  
  // Read tasks
  router.get("/tasks", (req, res) => {
    res.send("routing untuk read tasks");
  });
  
  // update tasks
  // put untuk edit atau mengganti semuanya
  // patch mengganti satu bagian
  
  router.put("/tasks", (req, res) => {
    res.send("routing untuk update tasks");
  });
  
  //  delete users
  app.delete("/tasks", (req, res) => {
    res.send("routing untuk delete tasks");
  });
  
module.exports = router;