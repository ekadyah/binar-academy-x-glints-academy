const express = require("express");

const app = express();


app.set("view engine", "ejs");
const profileRouter = require("./router/profile");
const articleRouter = require("./router/article");

app.get("/", (req, res) => res.send("ini home"));

app.use("/profile", profileRouter);

app.use("/article", articleRouter);

/**
 * article/all
 * article//find
 * article/search
 */

app.listen(5000, () => console.log("this app run in port 5000"));
