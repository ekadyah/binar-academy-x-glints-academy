// buat class controller dan diconversi jadi sebauh method dari router masing2

const profileModel = require("../model/profileModel");

class ProfileController {
  home(req, res) {
    res.send("ini page home dari page profile");
  }

  all(req, res) {
    const listProfiles = profileModel.getAllProfile();
    res.render(__dirname + "/view/profile/all.ejs", { listProfiles });
  }

  find(req, res) {
    const findProfile = profileModel.getFindProfile(req.params.number - 1);
    res.json(findProfile);
  }

  artikel(req, res) {
    const id = req.params.id ? req.params.id : "";
    res.send("ini page artikel " + id);
  }

  news(req, res) {
    res.send("ini page news dari page profile");
  }
}
//   search(req, res) {
//     res.send("ini page search dari page profile");
//   }
// }

module.exports = Object.freeze(new ProfileController());
