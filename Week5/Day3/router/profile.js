// belajar cara menggunakan function dari express dengan nama router
// ini hanya router bukan controller

const router = require('express').Router();
const ProfileController = require('../controller/profileController');

// router.get("/", (req, res) => res.send("ini home dari page profile"));
// diganti jadi
router.get("/", ProfileController.home);

// router.get("/all", (req, res) => res.send("ini all dari page profile"));
router.get("/all", ProfileController.all);

// router.get("/find", (req, res) => res.send("ini find dari page find"));
router.get("/find/:number", ProfileController.find);

// router.get('/artikel/:id?', (req, res) => {
// const id = req.params.id ? req.params.id : "";
// res.send('ini page artikel dari page profile '+ id );
// });
// menggunakan parameter agar dinamis tidak perlu membuat sebanyak router yg ada ? ternary operator
router.get("/artikel/:id?", (req, res) => {
    const id = req.params.id ? req.params. id : "";
    res.send("ini page artikel dari page profile " + id);
});

// router.get('/news', (req, res) => 
// res.send('ini page  news dari page profile'));
// jika mengakses url akan terbaca method get dan jika menggunakan method lain tidak akan terbaca

router.post("news", (req, res) => res.send ("ini page news dari profile"))

module.exports = router;