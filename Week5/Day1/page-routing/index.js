const express = require("express");
// var path = require("path");

const app = express();

app.get("/", (req, res) => {
  res.send("hello from express");
});

// app.get("/investment", (req, res) => {
//   res.sendFile(path.join(__dirname + "/investment.html"));
// });

app.get("/investment", (req, res) => {
  res.sendFile(__dirname + "/investment.html");
});

app.get("/news", (req, res) => {
  res.sendFile(__dirname + "/news.html");
});

app.get("/entrepreneur", (req, res) => {
  res.sendFile(__dirname + "/entrepreneur.html");
});

app.get("/market", (req, res) => {
  res.sendFile(__dirname + "/market.html");
});

app.get("/syariah", (req, res) => {
  res.sendFile(__dirname + "/syariah.html");
});

app.get("/tech", (req, res) => {
  res.sendFile(__dirname + "/tech.html");
});

app.get("/lifestyle", (req, res) => {
  res.sendFile(__dirname + "/lifestyle.html");
});

app.get("/insight", (req, res) => {
  res.sendFile(__dirname + "/insight.html");
});

app.get("/infografis", (req, res) => {
  res.sendFile(__dirname + "/infografis.html");
});

// app.patch("/", (req, res) => {
//   res.send("hello from express patch");
// });

// app.post("/", (req, res) => {
//   res.send("hello from express post");
// });

app.listen(4000, () => console.log("app running on port 4000"));
