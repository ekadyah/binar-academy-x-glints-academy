const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
require("dotenv").config();

const port = process.env.NODE_PORT;

const app = express();

function renameFile(oldNames) {
  const [filename, extentions] = oldNames.split(".");
  return `${filename}-${Date.now()}.${extentions}`;
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => cb(null, "/uploads"),
  filename: (req, file, cb) => cb(null, renameFile(file.originalname)),
});

const uploads = multer({ storage: storage });

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

// app.post("/register", (req, res) => res.send("hello"));

app.post("/register", uploads.single("profilePictures"), (req, res) => {
  res.send("success upload");
});

app.listen(port, () => console.log(`this app running on port ${port}`));
