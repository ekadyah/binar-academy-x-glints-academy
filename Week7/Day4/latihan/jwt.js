const express = require("express");
const bodyParser = require("body-parser");
const mongoose = require("mongoose");
const jwt = require("jsonwebtoken");
const bcrypt = require("bcrypt");
// koneksi ke server mongo db
mongoose.connect("mongodb://localhost:27017/latihan", {
  useNewUrlParser: true,
  useUnifiedTopology: true,
});

// Buat tabel
const UsersSchema = new mongoose.Schema({
  username: String,
  password: String,
  salt: String,
});

// Middleware Authentikasi
// Buat model users
const UsersModel = mongoose.model("users", UsersSchema);

// Middleware Authentikasi
const middlewareAuth = (req, res, next) => {
  try {
    // Split Bearer Token
    const token = req.headers.authorization.split(" ")[1];

    const decodedToken = jwt.verify(token, "bin4r4camd");

    res.locals.user - decodedToken;

    next();
  } catch (error) {
    res.status(400).json({ message: "token invalid" });
  }
};

const app = express();

// Nerima request dari req.body raw json
app.use(bodyParser.json());

// Nerima request dari req.body x-www-urlencoded
app.use(bodyParser.urlencoded({ extended: false }));

/**
 * Routing Login
 * Routing ini berguna untuk proses login dari users
 */

app.post("/register", async (req, res) => {
  const username = req.body.username;
  const password = req.body.password;

  // generate
  const saltKey = await bcrypt.genSalt(10);

  // merubah nilai menjadi password menjadi sebauh stirng hash
  const hashPassword = await bcrypt.hash(password, saltKey);

  // inpu data username
  await UsersModel.create({
    username: username,
    password: hashPassword,
    salt: saltKey,
  });

  // respon
  res.json({
    message: "sukses insert new users",
  });
});

app.post("/login", async (req, res) => {  
  let statusCode = 500;

  try {
    const username = req.body.username;
    const password = req.body.password;

    // cari data, ada ga
    const users = await UsersModel.findOne({ username: username });

    // melakukan pengecekan apakah ada atau tidak
    if (!users) {
      statusCode = 404;
      throw new Error("users not found");
    }
    // Membandingkan password dari req.body
    const isPasswordMatch = await bcrypt.compare(password, users.password);
    if (!isPasswordMatch) {
      statusCode = 400;
      throw new Error("password invalid");
    }

    // generate token
    const token = jwt.sign({ username: users.username }, "bin4r");

    // respon
    res.json({
      message: "success login",
      data: {
        token: token,
      },
    });
  } catch (error) {
    res.status(statusCode).json({ message: error.message });
  }
});

app.get("/users", middlewareAuth, (req, res) =>
  res.json({ message: "success get profile", data: res.locals.user })
);

app.listen(5200, () => console.log("this app running on port 5200"));
