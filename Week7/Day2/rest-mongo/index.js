const app = require("./src/app");
const mongoConnect = require("./src/mongoose");

(async () => {
  try {
    //start MongoDb cinnection
    await mongoConnect();
    // start apps
    app(5200);
  } catch (error) {
    console.log(error);
    console.log(`error app can't running`);
  }
})();