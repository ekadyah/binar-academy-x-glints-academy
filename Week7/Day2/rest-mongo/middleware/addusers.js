module.exports = function addUsers(req, res, next){

    // inisiasi allowed keys
  const AllowedKeys = ["fullName", "age"];

  const data = Object.keys(req.body);

  try {
      // empty request validation
      if(!data.length){
          throw new Error("request data can't be empty");
      }
      for(let i = 0; i < req.body.length; i++){ 
          const item = Object.keys(req.body[i]); 
         
          for(let j = 0; j < item.length; j++){
              if(!AllowedKeys.includes(item[j])) { 
              throw new Error("format request data is not valid")
          }
        }
      }
      next()
  } catch (error) {
    res.status(400).json({message : error.messege}); 
  }
};