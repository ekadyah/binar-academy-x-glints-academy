module.exports = function editUsers(req, res, next) {
  // inisiasi allowed keys
  const AllowedKeys = ["fullName", "age", "id"];

  const data = Object.keys(req.body);

  try {
    // empty request validation
    if (data.length !== 3) {
      throw new Error("data tidak sesuai format");
    }
    for (let i = 0; i < data.length; i++) {
      if (!AllowedKeys.includes(item[i])) {
        throw new Error(`data ${data[i]} tidak sesuai format`);
      }
    }
    next();
  } catch (error) {
    res.status(400).json({ message: error.messege });
  }
};
