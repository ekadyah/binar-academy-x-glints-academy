const router = require("express").Router();

const UsersModel = require("../models/users")();

const middlewareAddUsers = require("../middleware/addusers");

const middlewareEditUsers = require("../middleware/editUsers");

const middlewareDeleteUsers = require("../middleware/deleteusers");

module.exports = function routes() {
  router.get("/users", async (req, res) => {
    try {
      const data = await UsersModel.find({});
      res.json({ message: "success read new data users", data: data });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when read data users" });
    }
  });

  router.post("/users", async (req, res) => {
    try {
      await UsersModel.create(req.body);
      res.json({ message: "success create new data users" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when create data users" });
    }
  });

  router.put("/users", async (req, res) => {
    try {
      const id = req.body["_id"];
      const fullName = req.body["fullName"];
      const age = req.body["age"];
      await UsersModel.updateOne({ _id: id }, { fullName: fullName, age: age });
      res.json({ message: "success update data users" });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when update data users" });
    }
  });

  router.delete("/users", async (req, res) => {
    try {
      const id = req.query["id"];
      await UsersModel.deleteOne({ _id: id });
      res.json({ message: `success delete data users ${id}` });
    } catch (error) {
      console.log(error);
      res.status(500).json({ message: "error when delete data users" });
    }
  });                                                                                                                                                                                                                                               
  return router;
};
