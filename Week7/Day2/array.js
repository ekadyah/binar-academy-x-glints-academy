// 1
const data = { id: 1, name: "John", age: 20 };

const keysData = Object.keys(data); // ['id', 'name', 'age']

console.log(keysData);

// 2
const data2 = { id: 2, nama: "Budi", age: 30 };

const keysData2 = Object.keys(data2); // ['id, 'nama', 'age']

console.log(keysData2);

// mengecek apakah tipenya sesuai format atau tidak

for (let i = 0; i < keysData2.length; i++) {
  if (!keysData.includes(keysData2[i])) {
    console.log(`data keys ${keysData2[i]} tidak sesuai format`);
    break;
  }
}
