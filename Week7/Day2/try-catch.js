const mongoose = require("mongoose");

(async () => {
  //connec to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  // init mongose schema
  const Schema = mongoose.Schema;

  // create schema users (proses migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  // Create Models Users
  const UsersModel = mongoose.model("users", Users);

  //   // Insert New data
  const data = [
    {
      fullName: "Wenny",
      umur: 30,
    },
    {
      name: "Henny",
      age: 31,
    },
  ];

  // inisiasi allowed keys

  const AllowedKeys = ["fullName", "age"];

  try {
    // Looping check

    for (let index = 0; index < data.length; index++) {
      const requestKeys = Object.keys(data[index]);

      for (let index2 = 0; index2 < requestKeys.length; index2++) {
        if (!AllowedKeys.includes(requestKeys[index2])) {
          throw new Error(`data ${requestKeys[index2]} tidak sesuai format`);
        }
      }
    }

    await UsersMode.create(data);

    console.log("succes insert data to user schema");
  } catch (error) {
    console.log("ada yg error");
  }
})();
