const mongoose = require("mongoose");

(async () => {
  //connec to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  // init mongose schema
  const Schema = mongoose.Schema;

  // create schema users (proses migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  // Create Models Users
  const UsersModel = mongoose.model("users", Users);

  //   Delete
  await UsersModel.deleteOne({
    _id: "600ea5346b01da4fb697f481",
  });
  const data = await UsersModel.find({});
  console.log(data);
})();
