const mongoose = require("mongoose");

(async () => {
  //connec to mongodb server
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  // init mongose schema
  const Schema = mongoose.Schema;

  // create schema users (proses migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  // Create Models Users
  const UsersModel = mongoose.model("users", Users);

  //   // Insert New data
  const data = await UsersModel.create([
    { fullName: "Eka", age: 20 },
    { fullName: "Dyah", age: 21 },
    { fullName: "Cahyani", age: 22 },
    { fullName: "Amira", age: 23 },
    { fullName: "Inayatullah", age: 24 },
    { fullName: "Hikmah", age: 25 },
    { fullName: "Gilang", age: 26 },
    { fullName: "Afina", age: 27 },
    { fullName: "Adif", age: 28 },
    { fullName: "Iqbal", age: 29 },
  ]);
  console.log(data);
})();
